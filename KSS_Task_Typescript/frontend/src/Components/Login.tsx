import axios from 'axios';
import React, { useState } from 'react'
import { useNavigate } from 'react-router';
import 'react-notifications/lib/notifications.css';

import { apiUrl } from '../utils/constants';
import { loginProps } from '../Types';

const Login = () => {
    const [ email, setEmail ] = useState('');
    const [errors, setErrors] = useState<loginProps>({ email : '', password : ''});
    const [ password, setPassword ] = useState('');
    const navigate = useNavigate();

    const validateForm = () => {
        const error: loginProps = {};
            if (!email) {
                error.email = "Email is required!"
            }
            if (!password) {
                error.password = "Password is required!"
            }
            setErrors(error);
            return error;
    }
    const login = async () => {
        try {
            
            setErrors({});
            const error = await validateForm();
            if (Object.keys(error).length > 0) {
                return false
            }
            const config = {
                method: 'post',
                url: `${apiUrl}/users/login`,
                data : {
                    email,
                    password
                }
            }
            const res = await axios(config);
            console.log('res',res)
            if (res.data.code === 200) {
                await localStorage.setItem('token', JSON.stringify(res.data.token));
                setEmail("");
                setPassword("");
                // NotificationManager.success(
                //     null,
                //     res.data.message,
                //     3000,
                //     null,
                //     ''
                // );
                setTimeout(() => {
                    navigate('/')
                },3000)
            }
            else if (!res) {
                // NotificationManager.error(
                //     null,
                //     res.data.message,
                //     3000,
                //     null,
                //     ''
                // );
            }
        } catch (error) {
            console.log(error);
            // NotificationManager.error(
            //     null,
            //     'Wrong Email or Password',
            //     3000,
            //     null,
            //     ''
            // );
        }
    }

    return (
        <div className='container my-5 col-md-6'>
            <h3 className='text-center text-decoration-underline my-3'>Login Here To Continue</h3>
            <div className='border p-5'>
                <div className="mb-3">
                    <label htmlFor="exampleInputEmail1" className="form-label">Email address</label>
                    <input type="email" value={email} onChange={(e) => setEmail(e.target.value)} className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
                    {
                        errors.email && <p className='text-danger'>{errors.email}</p>
                    }
                </div>
                <div className="mb-3">
                    <label htmlFor="exampleInputPassword1" className="form-label">Password</label>
                    <input type="password" value={password} onChange={(e) => setPassword(e.target.value)}  className="form-control" id="exampleInputPassword1" />
                    {
                        errors.password && <p className='text-danger'>{errors.password}</p>
                    }
                </div>
                <div className='text-center'>
                    <button type="submit" className="btn btn-primary" onClick={login}>Submit</button>

                </div>
            </div>
            {/* <NotificationContainer /> */}
        </div>
    )
}

export default Login