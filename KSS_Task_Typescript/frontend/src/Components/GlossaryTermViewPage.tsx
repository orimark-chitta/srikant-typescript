import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router';
import { apiUrl } from '../utils/constants';
import 'react-notifications/lib/notifications.css';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import ImportExportIcon from '@mui/icons-material/ImportExport';

import { selectedTermProps } from '../Types';

const GlossaryTermViewPage = () => {
    const [terms, setTerms] = useState([]);
    const [selectedTerm, setSelectedTerm] = useState<selectedTermProps>({ term : '', definition : ''});
    const [term, setTerm] = useState('');
    const [definition, setDefinition] = useState('');
    const [token, setToken] = useState('');

    const navigate = useNavigate();

    const sortTerm = async (query:string) => {
        const config = {
            method: 'post',
            url: `${apiUrl}/glossaryTerms/sortAlphabatically`,
            headers: {
                'Authorization': `Bearer ${token}`,
                "Access-Control-Allow-Origin": "*"
            },
            data :{
                query : query
            }
        }
        const res = await axios(config);
        if (res.data.code === 200) {
            setTerms(res.data.terms);
        }
    }
    const fetchTerms = async (tokenData: string) => {
        const config = {
            method: 'get',
            url: `${apiUrl}/glossaryTerms/getTerm`,
            headers: {
                'Authorization': `Bearer ${tokenData}`,
                "Access-Control-Allow-Origin": "*"
            },
        }
        const res = await axios(config);
        if (res.data.code === 200) {
            setTerms(res.data.terms);
        }
    }

    const updateTerms = async () => {
        const config = {
            method: 'patch',
            url: `${apiUrl}/glossaryTerms/updateTerm/${selectedTerm?._id}`,
            headers: {
                'Authorization': `Bearer ${token}`,
                "Access-Control-Allow-Origin": "*"
            },
            data: {
                term: selectedTerm?.term,
                definition: selectedTerm?.definition
            }
        }
        const res = await axios(config);
        if (res.data.code === 200) {
            setTerm('');
            setDefinition('');
            await fetchTerms(token);
        }
    }

    const createTerms = async () => {
        const config = {
            method: 'post',
            url: `${apiUrl}/glossaryTerms/createTerm`,
            headers: {
                'Authorization': `Bearer ${token}`,
                "Access-Control-Allow-Origin": "*"
            },
            data: {
                term,
                definition
            }
        }
        const res = await axios(config);
        if (res.data.code === 200) {
            setTerm('');
            setDefinition('');
            await fetchTerms(token);
        }
    }
    const deleteTerms = async (id: string | null | undefined) => {
        const config = {
            method: 'delete',
            url: `${apiUrl}/glossaryTerms/deleteTerm/${id}`,
            headers: {
                'Authorization': `Bearer ${token}`,
                "Access-Control-Allow-Origin": "*"
            }
        }
        const res = await axios(config);
        if (res.data.code === 200) {
            await fetchTerms(token);
            // NotificationManager.error(
            //     null,
            //     res.data.message,
            //     3000,
            //     null,
            //     ''
            // );
        }
    }


    useEffect(() => {
        const fetchData = async () => {
            const tokenData = await localStorage.getItem('token') || '';
            const token = JSON.parse(tokenData);
            if (!token) {
                navigate('/login');
            }
            setToken(token)
            await fetchTerms(token);
        }
        fetchData();
    }, []);

    return (
        <div className='container mt-5'>
            <h2 className='my-3 text-decoration-underline text-center'>Glossary Terms</h2>
            <div className='text-end my-2'>
                <button className='btn btn-warning' data-bs-toggle="modal" data-bs-target="#createModal">Add New Term</button>
            </div>
            <div className='text-end my-2 row'>
                <div className='col-md-5'></div>
                <button className='btn btn-warning btn-sm m-1 col-md-3' onClick={()=>sortTerm('term')}>Sort Term Alphabetically <ImportExportIcon /></button>
                <button className='btn btn-warning btn-sm m-1 col-md-3' onClick={()=>sortTerm('definition')} >Sort Definition Alphabetically <ImportExportIcon /></button>
            </div>
            <table className="table table-sm my-3 table-hover">
                <thead className='bg-dark text-warning'>
                    <tr className='text-center'>
                        <th>
                            <span>Terms</span>
                        </th>
                        <th>Definition</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        terms?.map((term:selectedTermProps) => {
                            return (
                                <tr className='text-center' key={term._id}>
                                    <td>{term.term}</td>
                                    <td>{term.definition}</td>
                                    <td>
                                        <div className='d-flex justify-content-center align-items-center'>
                                            <button className='btn btn-outline-primary mx-1' data-bs-toggle="modal" data-bs-target="#updateModal" onClick={() => setSelectedTerm(term)}>Edit <EditIcon fontSize='small' /></button>
                                            <button className='btn btn-outline-danger mx-1' onClick={() => deleteTerms(term._id)}>Delete <DeleteIcon fontSize='small'/></button>
                                        </div>
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
            <div className="modal" tabIndex={-1} id='createModal'>
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title text-center">Create Glossary Term</h5>
                            <button type="button" className="btn-close create-btn" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                            <label className='my-2'>Term : </label>
                            <input type='text' value={term} onChange={(e) => setTerm(e.target.value)} placeholder="Term" className='form-control' />
                            <label className='my-2'>Definition : </label>
                            <input type='text' value={definition} onChange={(e) => setDefinition(e.target.value)} placeholder="Definition" className='form-control my-2' />
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-outline-danger" data-bs-dismiss="modal">Cancel</button>
                            <button type="button" className="btn btn-outline-success" onClick={createTerms}>Submit</button>
                        </div>
                    </div>
                </div>
            </div>
            <div className="modal" tabIndex={-1} id='updateModal'>
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title text-center">Update Glossary Term</h5>
                            <button type="button" className="btn-close update-btn" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                            <label className='my-2'>Term : </label>
                            <input type='text' value={selectedTerm.term} onChange={(e) => setSelectedTerm({ ...selectedTerm, term: e.target.value })} placeholder="Term" className='form-control' />
                            <label className='my-2'>Definition : </label>
                            <input type='text' value={selectedTerm.definition} onChange={(e) => setSelectedTerm({ ...selectedTerm, definition: e.target.value })} placeholder="Definition" className='form-control my-2' />
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-outline-danger" data-bs-dismiss="modal">Cancel</button>
                            <button type="button" className="btn btn-outline-success" onClick={updateTerms}>Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default GlossaryTermViewPage