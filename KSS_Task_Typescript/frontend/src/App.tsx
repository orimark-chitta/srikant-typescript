import { Route, Routes } from 'react-router-dom';
import './App.css';
import GlossaryTermViewPage from './Components/GlossaryTermViewPage';
import Login from './Components/Login';

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path='/' element={<GlossaryTermViewPage />} />
        <Route path='/login' element={<Login />} />
      </Routes>
    </div>
  );
}

export default App;
