export type GlossaryTermProps = {
    term : {
        term? : string,
        definition? : string,
        _id ? :string | undefined | null
    }[],
    
}
export type selectedTermProps = {
        term? : string,
        definition? : string,
        _id ? :string | undefined | null
}
export type loginProps = {
        email? : string,
        password? : string,
}