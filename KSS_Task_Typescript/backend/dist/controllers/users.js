"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.loginUser = exports.createUser = void 0;
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const bcryptjs_1 = __importDefault(require("bcryptjs"));
const users_1 = __importDefault(require("../models/users"));
const createUser = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const UserExists = yield users_1.default.findOne({ userName: req.body.userName });
    if (UserExists) {
        //   res.status(400)
        res.status(400).json({ message: 'User already exists' });
    }
    try {
        const salt = yield bcryptjs_1.default.genSalt(10);
        const hashedPassword = yield bcryptjs_1.default.hash(req.body.password, salt);
        const user = yield users_1.default.create(Object.assign(Object.assign({}, req.body), { password: hashedPassword }));
        if (user) {
            res.status(201).json({
                user,
                message: 'User successfully created.'
            });
        }
        else {
            res.status(400).json({
                message: 'Invalid data'
            });
        }
    }
    catch (error) {
        console.log(error);
        next(error);
    }
});
exports.createUser = createUser;
// export const deleteUser = async (req, res) => {
//   if (!req.body._id) {
//     return res.status(400).json({
//       code: 400,
//       message: 'Invalid _id!'
//     })
//   }
//   try {
//     const deletedUser = await User.findByIdAndDelete({ _id: req.body._id }, { new: true })
//     if (deletedUser) {
//       return res.status(200).json({
//         message: "User deleted successfully",
//         deletedUser,
//         code: 200,
//       })
//     };
//     return res.status(200).json({
//       message: "No User Found!!",
//       code: 404,
//     })
//   } catch (error) {
//     console.log(error)
//   }
// }
// export const getUsers = async (req, res) => {
//   const users = await User.find();
//   if (users.length > 0) {
//     res.status(200).json({
//       users,
//       message: 'Users successfully fetched.'
//     })
//   } else {
//     res.status(400).json({
//       message: 'No data found!'
//     })
//   }
// }
const loginUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const user = yield users_1.default.findOne({ email: req.body.email });
        if (!user) {
            return res.status(400).json({
                message: 'Wrong Password or Email',
                code: 400
            });
        }
        console.log(user);
        const userData = { user };
        const { __v, password } = user, data = __rest(user, ["__v", "password"]);
        const validPassword = yield bcryptjs_1.default.compare(req.body.password, `${user.password}`);
        if (!validPassword) {
            return res.status(400).json({
                message: 'Wrong Password or Email',
                code: 400
            });
        }
        else {
            const token = yield jsonwebtoken_1.default.sign(data, `${process.env.TOKEN_SECRET}`, { expiresIn: "2d" });
            return res.json({
                message: "Login Successfull",
                code: 200,
                data,
                token
            });
        }
    }
    catch (error) {
        return res.status(400).json({
            message: 'Invalid Email Id or Password!',
            code: 400,
            err: error.message
        });
    }
});
exports.loginUser = loginUser;
