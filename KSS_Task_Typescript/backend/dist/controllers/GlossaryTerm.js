"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getTermsById = exports.getTerms = exports.getSortedTerms = exports.deleteGlossaryTerm = exports.updateGlossaryTerm = exports.createGlossaryTerm = void 0;
const GlossaryTerm_1 = __importDefault(require("../models/GlossaryTerm"));
//CREATE
const createGlossaryTerm = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const termExists = yield GlossaryTerm_1.default.findOne({ definition: req.body.definition });
        if (termExists) {
            return res.status(400).json({ message: 'Glossary Term already exists' });
        }
        const term = yield GlossaryTerm_1.default.create(Object.assign({}, req.body));
        if (term) {
            res.status(201).json({
                term,
                message: 'Glossary Term successfully created.',
                code: 200
            });
        }
        else {
            res.status(400).json({
                message: 'Invalid data'
            });
        }
    }
    catch (error) {
        console.log(error);
        next({
            message: error.message,
            status: 500
        });
    }
});
exports.createGlossaryTerm = createGlossaryTerm;
//UPDATE
const updateGlossaryTerm = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const termExists = yield GlossaryTerm_1.default.findOne({ _id: req.params._id });
        if (termExists) {
            //   res.status(400);
            termExists.term = req.body.term;
            termExists.definition = req.body.definition;
            const updatedTerm = yield termExists.save();
            return res.status(200).json({
                message: 'Glossary Term updated successfully.',
                updatedTerm,
                code: 200
            });
        }
        else {
            return res.status(404).json({
                message: 'No Glossary Term Found!.',
                code: 200
            });
        }
    }
    catch (error) {
        console.log(error);
        next({
            message: error.message,
            status: 500
        });
    }
});
exports.updateGlossaryTerm = updateGlossaryTerm;
//DELETE
const deleteGlossaryTerm = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const deletedTerm = yield GlossaryTerm_1.default.findByIdAndDelete({ _id: req.params._id }, { new: true });
        if (deletedTerm) {
            return res.status(200).json({
                message: "Glossary Term deleted successfully",
                deletedTerm,
                code: 200,
            });
        }
        ;
        return res.status(404).json({
            message: "No Glossary Term Found!!",
            code: 404,
        });
    }
    catch (error) {
        console.log(error);
        next({
            message: error.message,
            status: 500
        });
    }
});
exports.deleteGlossaryTerm = deleteGlossaryTerm;
//Sort Term
const getSortedTerms = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const query = req.body.query;
    const terms = yield GlossaryTerm_1.default.find().sort(query.toLowerCase() === 'term' ? { term: 1 } : { definition: 1 });
    if (terms.length > 0) {
        res.status(200).json({
            terms,
            message: 'Terms successfully fetched.',
            code: 200
        });
    }
    else {
        next({
            message: 'No data found!',
            status: 404
        });
        // res.status(400).json({
        //     message: 'No data found!'
        // })
    }
});
exports.getSortedTerms = getSortedTerms;
//GET
const getTerms = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const terms = yield GlossaryTerm_1.default.find();
    if (terms.length > 0) {
        res.status(200).json({
            terms,
            message: 'Terms successfully fetched.',
            code: 200
        });
    }
    else {
        next({
            message: 'No data found!',
            status: 404
        });
        // res.status(400).json({
        //     message: 'No data found!'
        // })
    }
});
exports.getTerms = getTerms;
//GET BY ID
const getTermsById = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const terms = yield GlossaryTerm_1.default.findOne({ _id: req.params._id });
    if (terms) {
        res.status(200).json({
            terms,
            message: 'Term successfully fetched.'
        });
    }
    else {
        next({
            message: 'No data found!',
            status: 404
        });
    }
});
exports.getTermsById = getTermsById;
