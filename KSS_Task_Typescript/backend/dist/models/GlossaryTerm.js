"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const GlossaryTermSchema = new mongoose_1.default.Schema({
    term: {
        type: String,
        required: true
    },
    definition: {
        type: String,
        required: true
    }
}, {
    timestamps: true
});
const GlossaryTerm = mongoose_1.default.model('GlossaryTerm', GlossaryTermSchema);
exports.default = GlossaryTerm;
