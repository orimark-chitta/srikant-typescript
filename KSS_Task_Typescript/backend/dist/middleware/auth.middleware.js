"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.authUser = void 0;
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const authUser = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    // if (!req.validatedBody.api_key || req.validatedBody.api_key !== process.env.API_KEY) {
    //     return next({
    //         message : "Invalid Api_key",
    //         status : 400
    //     })
    // }
    let bearerToken = req.header('Authorization');
    if (!bearerToken || (bearerToken === null || bearerToken === void 0 ? void 0 : bearerToken.length) < 30) {
        return res.json({
            status: 'failed',
            code: 401,
            message: 'No token provided'
        });
    }
    else {
        bearerToken = bearerToken.split(' ')[1];
        try {
            const userData = yield jsonwebtoken_1.default.verify(bearerToken, `${process.env.TOKEN_SECRET}`);
            req.user = userData;
            req.token = bearerToken;
            next();
        }
        catch (error) {
            res.json({
                status: 'failed',
                code: 401,
                err: error.message,
                error: 'You are not authorized to access this resource'
            });
        }
    }
});
exports.authUser = authUser;
