"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const GlossaryTerm_1 = require("../controllers/GlossaryTerm");
const auth_middleware_1 = require("../middleware/auth.middleware");
// import { createTermValidator, updateTermValidator } from '../middleware/validator/Joi.validator.js';
const router = express_1.default.Router();
router.post('/createTerm', auth_middleware_1.authUser, GlossaryTerm_1.createGlossaryTerm);
router.patch('/updateTerm/:_id', auth_middleware_1.authUser, GlossaryTerm_1.updateGlossaryTerm);
router.delete('/deleteTerm/:_id', auth_middleware_1.authUser, GlossaryTerm_1.deleteGlossaryTerm);
router.get('/getTerm', auth_middleware_1.authUser, GlossaryTerm_1.getTerms);
router.get('/getTermById/:_id', auth_middleware_1.authUser, GlossaryTerm_1.getTerms);
router.post('/sortAlphabatically', auth_middleware_1.authUser, GlossaryTerm_1.getSortedTerms);
exports.default = router;
