import mongoose,{ connect} from 'mongoose';

export const database = async () => {
    try {
        mongoose.set('strictQuery', true);
        const db = connect(`${process.env.MONGO_URL}`);
        console.log('DB connected successfully.')
    } catch (error) {
        process.exit(1);
    }
}