import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';

import { Request, Response, NextFunction } from 'express';


import User from "../models/users";

export const createUser = async (req: Request, res: Response, next: NextFunction) => {

  const UserExists = await User.findOne({ userName: req.body.userName })

  if (UserExists) {
    //   res.status(400)
    res.status(400).json({ message: 'User already exists' });
  }

  try {
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);
    const user = await User.create({
      ...req.body,
      password: hashedPassword
    })
    if (user) {
      res.status(201).json({
        user,
        message: 'User successfully created.'
      })
    } else {
      res.status(400).json({
        message: 'Invalid data'
      })
    }

  } catch (error) {
    next(error);
  }
}

export const loginUser = async (req: Request, res: Response) => {

  try {
    const user = await User.findOne({ email: req.body.email });
  
    if (!user) {
      return res.status(400).json({
        message: 'Wrong Password or Email',
        code: 400
      })
    }
    
    const userData = { user };
    
    const { __v, password, ...data } = user;
    
    const validPassword = await bcrypt.compare(req.body.password, `${user.password}`);

    if (!validPassword) {
      return res.status(400).json({
        message: 'Wrong Password or Email',
        code: 400
      })
    }
    else {
      const token = await jwt.sign(data, `${process.env.TOKEN_SECRET}`, { expiresIn: "2d" });

      return res.json({
        message: "Login Successfull",
        code : 200,
        data,
        token
      })

    }
  } catch (error: any) {
      return res.status(400).json({
        message : 'Invalid Email Id or Password!',
        code : 400,
        err : error.message
      })
  }

}