
import { Request, Response, NextFunction } from 'express';
import GlossaryTerm from "../models/GlossaryTerm";


//CREATE

export const createGlossaryTerm = async (req:Request, res:Response, next:NextFunction) => {
    try {
        const termExists = await GlossaryTerm.findOne({ definition: req.body.definition })

        if (termExists) {
            return res.status(400).json({ message: 'Glossary Term already exists' });
        }

        const term = await GlossaryTerm.create({
            ...req.body,
        })

        if (term) {
            res.status(201).json({
                term,
                message: 'Glossary Term successfully created.',
                code : 200
            })
        } else {
            res.status(400).json({
                message: 'Invalid data'
            })
        }

    } catch (error:any) {
        next({
            message : error.message,
            status : 500
        });
    }
}

//UPDATE

export const updateGlossaryTerm = async (req: Request, res: Response, next:NextFunction) => {

    try {
        const termExists = await GlossaryTerm.findOne({ _id: req.params._id })

        if (termExists) {
            //   res.status(400);
            termExists.term = req.body.term;
            termExists.definition = req.body.definition;
            const updatedTerm = await termExists.save();
            return res.status(200).json({ 
                message: 'Glossary Term updated successfully.', 
                updatedTerm ,
                code : 200
            });
        }else {
            return res.status(404).json({ 
                message: 'No Glossary Term Found!.',
                code : 200
            });
        }
    } catch (error:any) {
        console.log(error);
        next({
            message : error.message,
            status : 500
        });
    }
}

//DELETE

export const deleteGlossaryTerm = async (req: Request, res: Response, next:NextFunction) => {

    try {
        const deletedTerm = await GlossaryTerm.findByIdAndDelete({ _id: req.params._id }, { new: true })
        if (deletedTerm) {
            return res.status(200).json({
                message: "Glossary Term deleted successfully",
                deletedTerm,
                code: 200,
            })
        };
        return res.status(404).json({
            message: "No Glossary Term Found!!",
            code: 404,
        })
    } catch (error:any) {
        next({
            message : error.message,
            status : 500
        });
    }
}

//Sort Term

export const getSortedTerms = async (req: Request, res: Response, next:NextFunction) => {
    const query = req.body.query;
    const terms = await GlossaryTerm.find().sort(query.toLowerCase() === 'term' ? {term : 1} : {definition : 1});

    if (terms.length > 0) {
        res.status(200).json({
            terms,
            message: 'Terms successfully fetched.',
            code : 200
        })
    } else {
        next({
            message : 'No data found!',
            status : 404
        });
        // res.status(400).json({
        //     message: 'No data found!'
        // })

    }
}
//GET

export const getTerms = async (req: Request, res: Response, next:NextFunction) => {
    const terms = await GlossaryTerm.find();

    if (terms.length > 0) {
        res.status(200).json({
            terms,
            message: 'Terms successfully fetched.',
            code : 200
        })
    } else {
        next({
            message : 'No data found!',
            status : 404
        });
        // res.status(400).json({
        //     message: 'No data found!'
        // })

    }
}
//GET BY ID

export const getTermsById = async (req:Request, res:Response, next : NextFunction) => {
    const terms = await GlossaryTerm.findOne({ _id: req.params._id });

    if (terms) {
        res.status(200).json({
            terms,
            message: 'Term successfully fetched.'
        })
    } else {
        next({
            message : 'No data found!',
            status : 404
        });

    }
}
