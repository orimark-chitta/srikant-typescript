import express, { Application, Request, Response, NextFunction } from 'express';
import cors from 'cors';
import * as dotenv from 'dotenv';

import userRoutes from './routes/users';
import { database } from './database/database';
import glossaryTermRoutes from './routes/GlossaryTerm';

const app: Application = express();
dotenv.config();
database();

//middlewares
app.use(express.json());
app.use(cors());
app.use('/users',userRoutes);
app.use('/glossaryTerms',glossaryTermRoutes);

app.listen(3024,(): void => {
    console.log('Server is running on port', 3024)
})