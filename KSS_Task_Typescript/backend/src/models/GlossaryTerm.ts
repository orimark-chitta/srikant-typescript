import mongoose from "mongoose";

interface GlossaryTerm {
    term :  String,
    definition : String
}

const GlossaryTermSchema = new mongoose.Schema<GlossaryTerm>({
    term : {
        type : String,
        required : true
    },
    definition : {
        type : String,
        required : true
    }
},{
    timestamps:true
})

const GlossaryTerm = mongoose.model('GlossaryTerm',GlossaryTermSchema);
export default GlossaryTerm;