import { model, Schema } from "mongoose";

// const userSchema = mongoose.Schema:any({
//     )

interface User{
    userName :  String,
    email : String,
    password : String,
    role : String,
}

const userSchema = new Schema<User>({
    userName : {
        type : String,
        required : true
    },
    email : {
        type : String,
        required : true
    },
    password : {
        type : String,
        required : true
    },
    role : {
        type : String,
        enum : ['user', 'admin'],
        required : true,
        default:'user'
    }
},{
    timestamps:true
}
)
const User = model('User',userSchema);
export default User;