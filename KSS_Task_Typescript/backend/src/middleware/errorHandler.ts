import { Request, Response, NextFunction } from 'express';
const errorHandler = async (Error?: any, req?: any, res?: any, next?: any) => {
    res.status(Error.status || 500);
    res.send({
        error : true,
        message : Error.message || "Internal Server Error"
    })
}
export default errorHandler;